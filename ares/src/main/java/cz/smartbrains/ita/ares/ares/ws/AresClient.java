package cz.smartbrains.ita.ares.ares.ws;

import cz.ares.response.AresOdpovedi;

public interface AresClient {
    AresOdpovedi getCompanyInfo(String ico);
}
