package cz.smartbrains.ita.ares.ares.model;

import lombok.Data;

@Data
public class AresAddressResponseDto {
    private String city;
    private String street;
    private String streetNumber;
    private String postalCode;
}
