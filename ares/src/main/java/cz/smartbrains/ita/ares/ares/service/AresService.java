package cz.smartbrains.ita.ares.ares.service;

import cz.smartbrains.ita.ares.ares.model.AresAddressResponseDto;

public interface AresService {
    AresAddressResponseDto getCompanyAddress(String ico);
}
