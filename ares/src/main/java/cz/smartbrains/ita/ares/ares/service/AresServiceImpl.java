package cz.smartbrains.ita.ares.ares.service;

import cz.smartbrains.ita.ares.ares.mapper.AresMapper;
import cz.smartbrains.ita.ares.ares.model.AresAddressResponseDto;
import cz.smartbrains.ita.ares.ares.ws.AresClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AresServiceImpl implements AresService {

    private final AresClient aresClient;
    private final AresMapper aresMapper;

    @Override
    public AresAddressResponseDto getCompanyAddress(String ico) {
        return aresMapper.mapToDto(aresClient.getCompanyInfo(ico).getOdpoved().get(0).getVypisRZP().get(0).getAdresy().getA().get(0));
    }
}
