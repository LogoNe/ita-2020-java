package cz.smartbrains.ita.ares.ares.mapper;


import cz.ares.response.AdresaARES;
import cz.smartbrains.ita.ares.ares.model.AresAddressResponseDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface AresMapper {
    @Mapping(source = "n", target = "city")
    @Mapping(source = "NU", target = "street")
    @Mapping(source = "CO", target = "streetNumber")
    @Mapping(source = "PSC", target = "postalCode")
    AresAddressResponseDto mapToDto(AdresaARES source);
}

