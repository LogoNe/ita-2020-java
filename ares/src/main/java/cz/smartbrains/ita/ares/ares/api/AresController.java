package cz.smartbrains.ita.ares.ares.api;

import cz.smartbrains.ita.ares.ares.model.AresAddressResponseDto;
import cz.smartbrains.ita.ares.ares.service.AresService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AresController {

    private final AresService aresService;

    @GetMapping(path = "/ares/{ico}")
    public AresAddressResponseDto test(@PathVariable String ico) {
        return aresService.getCompanyAddress(ico);
    }

}
