package cz.smartbrains.ita.ares.ares.configuration;


import cz.smartbrains.ita.ares.ares.ws.AresClient;
import cz.smartbrains.ita.ares.ares.ws.AresClientImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;


@Configuration
public class WsConfiguration {

    @Bean
    public Jaxb2Marshaller requestMarshaller(){
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("cz.ares.request");
        return marshaller;
    }

    @Bean
    public Jaxb2Marshaller responseMarshaller(){
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("cz.ares.response");
        return marshaller;
    }

    @Bean
    public AresClient icoClient(Jaxb2Marshaller requestMarshaller, Jaxb2Marshaller responseMarshaller){
        AresClientImpl client = new AresClientImpl();
        client.setDefaultUri("http://wwwinfo.mfcr.cz/cgi-bin/ares/xar.cgi");
        client.setMarshaller(requestMarshaller);
        client.setUnmarshaller(responseMarshaller);
        return client;
    }
}
