//package cz.smartbrains.ita.service;
//
//import cz.smartbrains.ita.domain.User;
//import cz.smartbrains.ita.exception.UserNotFoundException;
//import cz.smartbrains.ita.mapper.PhoneNumberMapperImpl;
//import cz.smartbrains.ita.mapper.UserMapper;
//import cz.smartbrains.ita.mapper.UserMapperImpl;
//import cz.smartbrains.ita.model.UserRequestDto;
//import cz.smartbrains.ita.model.UserResponseDto;
//import cz.smartbrains.ita.repository.UserRepository;
//import org.assertj.core.api.SoftAssertions;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mapstruct.factory.Mappers;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Spy;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.time.LocalDateTime;
//import java.util.List;
//import java.util.Optional;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.when;
//
//@ExtendWith(MockitoExtension.class)
//@SpringBootTest(classes = {UserServiceImpl.class, UserMapperImpl.class, PhoneNumberMapperImpl.class})
//class UserServiceImplTest {
//    @Autowired
//    private UserMapperImpl userMapper;
//
//    @Mock
//    private UserRepository userRepository;
//
//
//    @InjectMocks
//    private UserServiceImpl userService;
//
//    @Test
//    void saveUser() {
//        UserRequestDto userRequestDto = new UserRequestDto();
//        userRequestDto.setFirstName("first");
//        userRequestDto.setLastName("last");
//        userRequestDto.setLogin("login");
//        userRequestDto.setPhone("phone");
//
//        User user = userMapper.mapToDomain(userRequestDto);
//
//        when(userRepository.save(any(User.class)))
//                .thenReturn(user);
//        UserResponseDto result = userService.saveUser(userRequestDto);
//        SoftAssertions assertions = new SoftAssertions();
//
//        assertThat(result).isNotNull();
//
//        assertions.assertThat(result.getFirstName()).isEqualTo(userRequestDto.getFirstName());
//        assertions.assertThat(result.getLastName()).isEqualTo(userRequestDto.getLastName());
//        assertions.assertThat(result.getLogin()).isEqualTo(userRequestDto.getLogin());
//        assertions.assertThat(result.getPhone()).isEqualTo(userRequestDto.getPhone());
//
//        assertions.assertAll();
//    }
//
//    @Test
//    void getAllUsers() {
//        User user = new User();
//        user.setFirstName("first");
//        user.setLastName("last");
//        user.setLogin("login");
//        user.setPhone("phone");
//
//        User user1 = new User();
//        user.setFirstName("first2");
//        user.setLastName("last2");
//        user.setLogin("login2");
//        user.setPhone("phone2");
//
//
//       when(userRepository.findAll())
//               .thenReturn(List.of(user,
//                       user1));
//        List<UserResponseDto> result = userService.getAllUsers();
//
//        SoftAssertions assertions = new SoftAssertions();
//
//        assertThat(result).isNotNull();
//
//        assertions.assertThat(result.get(0).getFirstName()).isEqualTo(user.getFirstName());
//        assertions.assertThat(result.get(0).getLastName()).isEqualTo(user.getLastName());
//        assertions.assertThat(result.get(0).getLogin()).isEqualTo(user.getLogin());
//        assertions.assertThat(result.get(0).getPhone()).isEqualTo(user.getPhone());
//
//        assertions.assertThat(result.get(1).getFirstName()).isEqualTo(user1.getFirstName());
//        assertions.assertThat(result.get(1).getLastName()).isEqualTo(user1.getLastName());
//        assertions.assertThat(result.get(1).getLogin()).isEqualTo(user1.getLogin());
//        assertions.assertThat(result.get(1).getPhone()).isEqualTo(user1.getPhone());
//
//        assertions.assertAll();
//    }
//
//
//    @Test
//    void findByLogin() {
//        User user = new User();
//        user.setFirstName("first");
//        user.setLastName("last");
//        user.setLogin("login");
//        user.setPhone("phone");
//
//        String login = "login";
//        when(userRepository.findByLogin(user.getLogin()))
//                .thenReturn(Optional.of(user));
//        UserResponseDto result = userService.findByLogin(user.getLogin());
//
//        SoftAssertions assertions = new SoftAssertions();
//
//        assertions.assertThat(result.getFirstName()).isEqualTo(user.getFirstName());
//        assertions.assertThat(result.getLastName()).isEqualTo(user.getLastName());
//        assertions.assertThat(result.getLogin()).isEqualTo(user.getLogin());
//        assertions.assertThat(result.getPhone()).isEqualTo(user.getPhone());
//
//        assertions.assertAll();
//    }
//
//    @Test
//    void findByLogin_userNotFound() {
//        String login = "login";
//        when(userRepository.findByLogin(login))
//                .thenReturn(Optional.empty());
//
//        UserNotFoundException userNotFoundException = assertThrows(UserNotFoundException.class, () -> userService.findByLogin(login));
//
////        assertThat(userNotFoundException.getMessage()).isEqualTo("User not found, user does not exist");
//    }
//
//    @Test
//    void updateUser_userNotFound() {
//        UserRequestDto userRequestDto = new UserRequestDto();
//        userRequestDto.setFirstName("first");
//        userRequestDto.setLastName("last");
//        userRequestDto.setLogin("login");
//        userRequestDto.setPhone("phone");
//
//
//        when(userRepository.findByLogin(userRequestDto.getLogin()))
//                .thenReturn(Optional.empty());
//
//        UserNotFoundException userNotFoundException = assertThrows(UserNotFoundException.class, () -> userService.updateUser(userRequestDto));
//
//        SoftAssertions assertions = new SoftAssertions();
//
////        assertions.assertThat(userNotFoundException.getMessage()).isEqualTo("User not found, can't update user");
//        assertions.assertThat(userNotFoundException.getStatus().value()).isEqualTo(404);
//        assertions.assertAll();
//    }
//
//    @Test
//    void updateUser() {
//        UserRequestDto userRequestDto = new UserRequestDto();
//        userRequestDto.setFirstName("firstChanged");
//        userRequestDto.setLastName("lastChanged");
//        userRequestDto.setLogin("login");
//        userRequestDto.setPhone("phoneChanged");
//
//        User user = new User();
//        user.setFirstName("first");
//        user.setLastName("last");
//        user.setLogin("login");
//        user.setPhone("phone");
//
//
//        String login = "login";
//
//        when(userRepository.findByLogin(login))
//                .thenReturn(Optional.of(user));
//
//        UserResponseDto result = userService.updateUser(userRequestDto);
//
//
//        SoftAssertions assertions = new SoftAssertions();
//        assertions.assertThat(result.getFirstName()).isEqualTo(user.getFirstName());
//        assertions.assertThat(result.getLastName()).isEqualTo(user.getLastName());
//        assertions.assertThat(result.getLogin()).isEqualTo(user.getLogin());
//        assertions.assertThat(result.getPhone()).isEqualTo(user.getPhone());
//        assertions.assertAll();
//    }
//
//    @Test
//    void deleteUser(){
//        User user = new User();
//        user.setFirstName("first");
//        user.setLastName("last");
//        user.setLogin("login");
//        user.setPhone("phone");
//
//        when(userRepository.findByLogin(user.getLogin()))
//                .thenReturn(Optional.of(user));
//
//        String result = userService.delteUser(user.getLogin());
//
//        SoftAssertions assertions = new SoftAssertions();
//        assertions.assertThat(result).isNotNull();
//        assertions.assertThat(result).isEqualTo("Ok");
//    }
//
//    @Test
//    void delteUser_userNotFound() {
//        String login = "login";
//        when(userRepository.findByLogin(login))
//                .thenReturn(Optional.empty());
//
//        UserNotFoundException userNotFoundException = assertThrows(UserNotFoundException.class, () -> userService.delteUser(login));
//
//        SoftAssertions assertions = new SoftAssertions();
//
////        assertions.assertThat(userNotFoundException.getMessage()).isEqualTo("User not found, can't delete user");
//        assertions.assertThat(userNotFoundException.getStatus().value()).isEqualTo(404);
//        assertions.assertAll();
//    }
//}