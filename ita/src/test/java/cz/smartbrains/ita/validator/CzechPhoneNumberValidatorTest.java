package cz.smartbrains.ita.validator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.assertj.core.api.Assertions.assertThat;


class CzechPhoneNumberValidatorTest {

    CzechPhoneNumberValidator validator = new CzechPhoneNumberValidator();

    @ParameterizedTest
    @MethodSource
    void isValid(String number) {
        boolean result = validator.isValid(number, null);
        assertThat(result).isTrue();
    }
    static Object[] isValid(){
        return new Object[]{
                "+420123456789", //preffix
                "123456789" //no preffix
        };
    }

    @ParameterizedTest
    @MethodSource
    void isFalse(String number) {
        boolean result = validator.isValid(number, null);
        assertThat(result).isFalse();
    }
    static Object[] isFalse(){
        return new Object[]{
                "+420123s45678", //letter
                "1234567891", //lengh test, too long
                "+42012345678", //too short
                null, //is null
                "", //empty
                "420123456789" //without +
        };
    }
}