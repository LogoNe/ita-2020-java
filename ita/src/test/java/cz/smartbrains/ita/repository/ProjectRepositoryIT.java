package cz.smartbrains.ita.repository;

import cz.smartbrains.ita.domain.*;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;


import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureTestEntityManager
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ProjectRepositoryIT {

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TestEntityManager entityManager;



    @Test
    @Transactional
    void create() {
        Project project = new Project()
                .setName("projectName")
                .setPrice((long) 1)
                .setStartDate("today")
                .setEndDate("tomorrow")
                .setEmployees(Set.of(new Employee()
                        .setFirstName("testEmployee")))
                .setCompany(new Company()
                        .setName("testCompany"));

        projectRepository.create(project);

        Project result = entityManager.find(Project.class, project.getId());

        assertThat(result).isNotNull();

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(result.getName()).isEqualTo(project.getName());
        softAssertions.assertThat(result.getEmployees().iterator().next().getFirstName())
                .isEqualTo(project.getEmployees().iterator().next().getFirstName());
        softAssertions.assertThat(result.getStartDate()).isEqualTo(project.getStartDate());
        softAssertions.assertThat(result.getPrice()).isEqualTo(project.getPrice());
        softAssertions.assertThat(result.getCompany().getName()).isEqualTo(project.getCompany().getName());
    }


    @Test
    @Transactional
    void findByNameAndDate(){
        LocalDateTime creationDateTime = LocalDateTime.now();
        Project project = (Project) new Project()
                .setName("project")
                .setCreatedAt(creationDateTime);

        entityManager.persistAndFlush(project);
        entityManager.detach(project);

        List<Project> result = projectRepository.findAll("project", creationDateTime);

        assertThat(result).isNotNull();
        SoftAssertions assertions = new SoftAssertions();
        assertions.assertThat(result.get(0).getName()).isEqualTo(project.getName());

        //asi neni treba
//        assertions.assertThat(result.get(0).getCreatedAt()).isEqualTo(project.getCreatedAt());
        assertions.assertAll();
    }

}