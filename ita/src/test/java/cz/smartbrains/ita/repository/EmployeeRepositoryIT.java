package cz.smartbrains.ita.repository;

import cz.smartbrains.ita.domain.Company;
import cz.smartbrains.ita.domain.Employee;
import cz.smartbrains.ita.domain.Project;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@AutoConfigureTestEntityManager
class EmployeeRepositoryIT {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private EmployeeRepository employeeRepository;


    @Test
    @Transactional
    void createEmployee() {

        Employee employee = new Employee()
                .setFirstName("employeeName")
                .setLastName("employeeLastName")
                .setProjects(Set.of(new Project().setName("project")))
                .setSalary((long) 1);

        employeeRepository.createEmployee(employee);

        Employee result = entityManager.find(Employee.class, employee.getId());

        assertThat(result).isNotNull();

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(result.getFirstName()).isEqualTo(employee.getFirstName());
        softAssertions.assertThat(result.getLastName()).isEqualTo(employee.getLastName());
        softAssertions.assertThat(result.getSalary()).isEqualTo(employee.getSalary());
        softAssertions.assertThat(result.getProjects().iterator().next().getName())
                .isEqualTo(employee.getProjects().iterator().next().getName());
        softAssertions.assertAll();
    }
}