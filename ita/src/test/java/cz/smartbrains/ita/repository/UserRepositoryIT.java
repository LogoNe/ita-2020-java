package cz.smartbrains.ita.repository;

import cz.smartbrains.ita.domain.PhoneNumber;
import cz.smartbrains.ita.domain.User;
import cz.smartbrains.ita.mapper.PhoneNumberMapper;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class UserRepositoryIT {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void saveEntity() {
        User user = new User();
        PhoneNumber phoneNumber = new PhoneNumber();
        // kdyz mam lombokovy setry a getry pres data, tak setter je typ clasy a vraci sam sebe.
        // a pak mi to hlasi void not allowed in this method.
        phoneNumber.setPhoneNumber("test");
//        phoneNumber.setId((long 1);
        user.setPhoneNumbers(Set.of(phoneNumber));
        user.setFirstName("first");
        user.setLastName("last");
        user.setLogin("login");
        user.setPhone("phone0");
        
        entityManager.persistAndFlush(user);
        entityManager.detach(user);

        final Optional<User> result = userRepository.findById(user.getId());

        assertThat(result.isPresent()).isTrue();
        SoftAssertions.assertSoftly(softly -> {
            assertThat(result.get().getLogin()).isEqualTo(user.getLogin());
            assertThat(result.get().getFirstName()).isEqualTo(user.getFirstName());
            assertThat(result.get().getLastName()).isEqualTo(user.getLastName());
            assertThat(result.get().getPhone()).isEqualTo(user.getPhone());
            assertThat(result.get().getPhoneNumbers().iterator().next().getPhoneNumber())
                    .isEqualTo(user.getPhoneNumbers().iterator().next().getPhoneNumber());
        });

    }

    @Test
    void findByLogin() {
    }

    @Test
    void findAll() {
    }
}