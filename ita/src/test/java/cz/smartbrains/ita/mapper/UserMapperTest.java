package cz.smartbrains.ita.mapper;

import cz.smartbrains.ita.domain.PhoneNumber;
import cz.smartbrains.ita.domain.User;
import cz.smartbrains.ita.model.PhoneNumberDto;
import cz.smartbrains.ita.model.UserRequestDto;
import cz.smartbrains.ita.model.UserResponseDto;
import org.apache.tomcat.jni.Local;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest(classes = {UserMapperImpl.class, PhoneNumberMapperImpl.class})
class UserMapperTest {

    @Autowired
    private UserMapperImpl userMapper;

    @Test
    void mapToResponseDto() {
        User user = new User();
        user.setFirstName("first");
        user.setLastName("last");
        user.setLogin("login");
        user.setPhone("phone");
        user.setCreatedAt(LocalDateTime.now());

        PhoneNumber tel1 = new PhoneNumber();
        PhoneNumber tel2 = new PhoneNumber();
        tel1.setPhoneNumber("phone1");
        tel2.setPhoneNumber("phone2");
        user.setPhoneNumbers(Set.of(tel1, tel2));

        UserResponseDto result = userMapper.mapToResponseDto(user);

        assertThat(result).isNotNull();

        SoftAssertions assertions = new SoftAssertions();

        assertions.assertThat(result.getFirstName()).isEqualTo(user.getFirstName());
        assertions.assertThat(result.getLastName()).isEqualTo(user.getLastName());
        assertions.assertThat(result.getLogin()).isEqualTo(user.getLogin());
        assertions.assertThat(result.getPhone()).isEqualTo(user.getPhone());
        assertions.assertThat(result.getCreatedAt()).isEqualTo(user.getCreatedAt());
//        assertions.assertThat(result.getPhoneNumbers().).isEqualTo(Set.of(tel1, tel2))

        assertions.assertAll();
    }

    @Test
    void mapToDomain() {
        UserRequestDto user = new UserRequestDto();
        user.setFirstName("first");
        user.setLastName("last");
        user.setLogin("login");
        user.setPhone("phone");
        user.setPhoneNumbers(Set.of(new PhoneNumberDto()));


        User result = userMapper.mapToDomain(user);

        assertThat(result).isNotNull();

        SoftAssertions assertions = new SoftAssertions();

        assertions.assertThat(result.getFirstName()).isEqualTo(user.getFirstName());
        assertions.assertThat(result.getLastName()).isEqualTo(user.getLastName());
        assertions.assertThat(result.getLogin()).isEqualTo(user.getLogin());
        assertions.assertThat(result.getPhone()).isEqualTo(user.getPhone());

        assertions.assertAll();
    }
}