package cz.smartbrains.ita.api;

import cz.smartbrains.ita.exception.ItaException;
import cz.smartbrains.ita.exception.UserAlreadyExistsException;
import cz.smartbrains.ita.exception.UserNotFoundException;
import cz.smartbrains.ita.model.ExceptionResponseDto;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.spi.ViolatedConstraintNameExtracter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
   @ExceptionHandler(value = {ItaException.class})
    protected ResponseEntity<Object> handleItaExceptions(ItaException itaEx, WebRequest request) {
           return handleExceptionInternal(itaEx, buildResponse(itaEx), new HttpHeaders(), itaEx.getStatus(), request);
    }

    @ExceptionHandler(value = {RuntimeException.class})
    protected ResponseEntity<Object> handleExceptions(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    private ExceptionResponseDto buildResponse(ItaException exception){
        return new ExceptionResponseDto(exception.getMessage(), exception.getStatus().value(), LocalDateTime.now());
    }

}

