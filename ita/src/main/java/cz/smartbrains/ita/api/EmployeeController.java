package cz.smartbrains.ita.api;

import cz.smartbrains.ita.model.CompanyWithBranchesWithEmployeesResponseDto;
import cz.smartbrains.ita.model.EmployeeRequestDto;
import cz.smartbrains.ita.model.EmployeeResponseDto;
import cz.smartbrains.ita.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeService employeeService;

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @PostMapping(path = "/employee")
    public EmployeeResponseDto createEmployee(@RequestBody EmployeeRequestDto employeeRequestDto){
        return employeeService.createEmployee(employeeRequestDto);
    }

}
