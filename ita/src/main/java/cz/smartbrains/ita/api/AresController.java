package cz.smartbrains.ita.api;

import cz.smartbrains.ita.client.AresMicroClient;
import cz.smartbrains.ita.model.AresAddressResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AresController {

    private final AresMicroClient aresClient;

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping(path = "ares/{ico}")
    public AresAddressResponseDto getCompanyAddressByVatAd(@PathVariable String ico) {
        return aresClient.getCompanyInfoByVatId(ico);
    }

}
