package cz.smartbrains.ita.api;

import cz.smartbrains.ita.domain.Company;
import cz.smartbrains.ita.domain.Employee;
import cz.smartbrains.ita.domain.Project;
import cz.smartbrains.ita.model.*;
import cz.smartbrains.ita.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Secured({"ROLE_ADMIN","ROLE_USER"})
    @PostMapping(path = "/user")
    public UserResponseDto saveUser(@Valid @RequestBody UserRequestDto user){
        return userService.saveUser(user);
    }

    @Secured("ROLE_ADMIN")
    @GetMapping(path = "/user")
    public List<UserResponseDto> getAllUsers(){
        return userService.getAllUsers();
    }

    @Secured({"ROLE_ADMIN","ROLE_USER"})
    @GetMapping(path = "/user/{login}")
    public UserResponseDto findByLogin(@PathVariable("login") String login){
        return userService.findByLogin(login);
    }

    @Secured({"ROLE_ADMIN","ROLE_USER"})
    @DeleteMapping(path = "/user/{login}")
    public String deleteUser(@PathVariable("login") String login){

        return userService.delteUser(login);

    }

    @Secured({"ROLE_ADMIN","ROLE_USER"})
    @PutMapping(path = "/user/")
    public UserResponseDto alterUser(@Valid @RequestBody UserRequestDto user) {
        return userService.updateUser(user);
    }

}
