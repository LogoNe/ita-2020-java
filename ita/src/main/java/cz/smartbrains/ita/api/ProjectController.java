package cz.smartbrains.ita.api;

import cz.smartbrains.ita.domain.Project;
import cz.smartbrains.ita.model.ProjectRequestDto;
import cz.smartbrains.ita.model.ProjectResponseDto;
import cz.smartbrains.ita.service.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/project")
@RequiredArgsConstructor
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @Secured({"ROLE_ADMIN","ROLE_USER"})
    @GetMapping
    public List<ProjectResponseDto> findAllProjects(){
        return projectService.findALl();
    }

    @Secured({"ROLE_ADMIN","ROLE_USER"})
    @GetMapping(path = "/project")
    public List<ProjectResponseDto> findSpecificProjects(@RequestBody ProjectRequestDto project){
        return projectService.findAll(project);
    }

    @Secured({"ROLE_ADMIN","ROLE_USER"})
    @PostMapping
    public ProjectResponseDto createProject(@RequestBody ProjectRequestDto project){
        return projectService.createProject(project);
    }

    @Secured({"ROLE_ADMIN","ROLE_USER"})
    @PutMapping
    public ProjectResponseDto updateProject(@RequestBody ProjectRequestDto project){
        return projectService.updateProject(project);
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @DeleteMapping(path = "/project/{projectName}")
    public String deleteProject(@PathVariable("projectName") String projectName){
        return projectService.deleteProject(projectName);
    }
}
