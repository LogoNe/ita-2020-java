package cz.smartbrains.ita.api;

import cz.smartbrains.ita.domain.Company;
import cz.smartbrains.ita.model.CompanyWithBranchesWithEmployeesResponseDto;
import cz.smartbrains.ita.model.CompanyWithCompanyBranchesResponseDto;
import cz.smartbrains.ita.service.CompanyService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/company")
@RequiredArgsConstructor
public class CompanyController {

    private final CompanyService companyService;

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping(path = "/companyDto/{companyId}")
    public CompanyWithCompanyBranchesResponseDto getAllCompaniesByIdDto(@PathVariable("companyId") Long companyId){
        return companyService.findCompanyById(companyId);
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping(path = "/companyDto")
    public List<CompanyWithCompanyBranchesResponseDto> getAllCompaniesDto(){
        return companyService.findAllDto();
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping(path = "/employee/{companyId}")
    public CompanyWithBranchesWithEmployeesResponseDto getEmployeesByCompanyId(@PathVariable("companyId") Long companyId){
        return companyService.findEmployeesByCompanyId(companyId);
    }
}
