package cz.smartbrains.ita.service;

import cz.smartbrains.ita.domain.Employee;
import cz.smartbrains.ita.model.*;

import java.util.List;

public interface EmployeeService {
    /**
     * Creates an employee based on employeeRequestDTO
     * @param employeeRequestDto employee to be created
     * @return a created employee
     */
    EmployeeResponseDto createEmployee(EmployeeRequestDto employeeRequestDto);

    /**
     * Updates an employee to match employeeRequestDto
     * @param employeeRequestDto employee to update
     * @return an updated employee
     */
    EmployeeResponseDto updateEmployee(EmployeeRequestDto employeeRequestDto);

    /**
     * Deletes an employee that matches employeeRequestDto
     * @param employeeRequestDto employee to delete
     */
    void deleteEmployee(EmployeeRequestDto employeeRequestDto);
}
