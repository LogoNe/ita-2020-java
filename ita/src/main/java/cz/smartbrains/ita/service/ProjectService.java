package cz.smartbrains.ita.service;

import cz.smartbrains.ita.domain.Project;
import cz.smartbrains.ita.model.ProjectRequestDto;
import cz.smartbrains.ita.model.ProjectResponseDto;

import java.sql.Timestamp;
import java.util.List;

public interface ProjectService {
    /**
     * finds all projects and returns list of ProjectResponseDtos
     * @return all projects
     */
    List<ProjectResponseDto> findALl();

    /**
     * Finds all projects which were created after specific date and match the name
     * @param projectRequestDto specific project
     * @return specific project
     */
    List<ProjectResponseDto> findAll(ProjectRequestDto projectRequestDto);

    /**
     * Creates a project based on a projectRequestDto
     * @param projectRequestDto project to create
     * @return a created project
     */
    ProjectResponseDto createProject(ProjectRequestDto projectRequestDto);

    /**
     * Finds and updates project to match projectRequestDto
     * @param projectRequestDto specific project
     * @return updated project
     */
    ProjectResponseDto updateProject(ProjectRequestDto projectRequestDto);

    /**
     * Deletes a project based on a name
     * @param projectName name of the project
     * @return OK - if project exists
     */
    String deleteProject(String projectName);
}
