package cz.smartbrains.ita.service.impl;

import cz.smartbrains.ita.domain.Company;
import cz.smartbrains.ita.mapper.CompanyMapper;
import cz.smartbrains.ita.model.CompanyWithBranchesWithEmployeesResponseDto;
import cz.smartbrains.ita.model.CompanyWithCompanyBranchesResponseDto;
import cz.smartbrains.ita.repository.CompanyRepository;
import cz.smartbrains.ita.service.CompanyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class CompanyServiceImpl implements CompanyService {

    private final CompanyRepository companyRepository;
    private final CompanyMapper companyMapper;

    @Override
    public List<CompanyWithCompanyBranchesResponseDto> findAllDto(){
        return companyRepository.findAll().stream()
                .map(companyMapper::mapToCompanyWithBranchesDto)
                .collect(Collectors.toList());

    }
    @Override
    public CompanyWithBranchesWithEmployeesResponseDto findEmployeesByCompanyId(Long comapnyId) {
        return companyMapper.mapToCompanyWithBranchesWithEmployees(companyRepository.findCompanyById(comapnyId));
    }

    @Override
    public CompanyWithCompanyBranchesResponseDto findCompanyById(Long companyId){
        return companyMapper.mapToCompanyWithBranchesDto(companyRepository.findCompanyById(companyId));
    }
}
