package cz.smartbrains.ita.service.impl;

import cz.smartbrains.ita.domain.User;
import cz.smartbrains.ita.exception.UserNotFoundException;
import cz.smartbrains.ita.mapper.UserMapper;
import cz.smartbrains.ita.model.UserRequestDto;
import cz.smartbrains.ita.model.UserResponseDto;
import cz.smartbrains.ita.repository.UserRepository;
import cz.smartbrains.ita.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

//    @Autowired
    private final UserRepository userRepository;
    private final UserMapper userMapper;

//    @Autowired
//    private ProjectRepository projectRepository;
//    private final ProjectMapper projectMapper;

    @Override
    @Transactional
    public UserResponseDto saveUser(UserRequestDto userRequest) {
        log.info("Method: saveUser called");
//        User user1 = userRepository.save(userMapper.mapToDomain(userRequest));
        User user = userMapper.mapToDomain(userRequest);
        UserResponseDto userResponseDto = userMapper.mapToResponseDto(userRepository.save(user));
        log.debug("input {}, output {} saved",userRequest );
        return userResponseDto;
    }

    @Override
    public List<UserResponseDto> getAllUsers() {
        log.info("Method: getAllUsers called");
        List<UserResponseDto> collect = userRepository.findAll().stream()
                .map(userMapper::mapToResponseDto)
                .collect(Collectors.toList());
        log.debug("output {} found", collect);
        return collect;
    }

    @Override
    public UserResponseDto findByLogin(String login) {
        log.info("Method: findAllByLogin called");
        UserResponseDto userResponseDto = userMapper.mapToResponseDto(userRepository.findByLogin(login)
                .orElseThrow(() -> new UserNotFoundException("User not found, user does not exist")));
        log.debug("input {} output {}", login, userResponseDto);
        return userResponseDto;
    }


    @Override
    @Transactional
    public UserResponseDto updateUser(UserRequestDto user) {
        log.info("Method: updateUser called");
        String username;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }

        if(user.getLogin().equals(username)){
            User userToUpdate = userRepository.findByLogin(user.getLogin())
                    .orElseThrow(()-> new UserNotFoundException("User not found, can't update user"));

            userToUpdate.setFirstName(user.getFirstName());
            userToUpdate.setLastName(user.getLastName());
            userToUpdate.setPhone(user.getPhone());

            UserResponseDto UserResponseDto = userMapper.mapToResponseDto(userToUpdate);
            log.debug("input {}, output {} ", user, UserResponseDto);
            return UserResponseDto;

        }else throw new SecurityException(username + " can't access " + user.getLogin());

    }

    @Override
    @Transactional
    public String delteUser(String login) {
        log.info("Method: delteUser called");

        String username;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
             username = ((UserDetails)principal).getUsername();
        } else {
             username = principal.toString();
        }
        if(login.equals(username)){
            User userToDelete = userRepository.findByLogin(login)
                    .orElseThrow(()-> new UserNotFoundException("User not found, can't delete user"));
            log.debug("input {}, output {} deleted", login, userToDelete);
            userRepository.delete(userToDelete);
            return "Ok";
        }else throw new SecurityException(username + " can't access " + login);
    }
}