package cz.smartbrains.ita.service.impl;

import cz.smartbrains.ita.domain.Project;
import cz.smartbrains.ita.domain.User;
import cz.smartbrains.ita.exception.UserNotFoundException;
import cz.smartbrains.ita.mapper.ProjectMapper;
import cz.smartbrains.ita.model.ProjectRequestDto;
import cz.smartbrains.ita.model.ProjectResponseDto;
import cz.smartbrains.ita.model.UserResponseDto;
import cz.smartbrains.ita.repository.ProjectCrudRepo;
import cz.smartbrains.ita.repository.ProjectRepository;
import cz.smartbrains.ita.service.ProjectService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProjectServiceImpl implements ProjectService {
    private final ProjectMapper projectMapper;
    private final ProjectRepository projectRepository;
    private final ProjectCrudRepo projectCrudRepo;


    @Override
    public List<ProjectResponseDto> findALl() {
        List<ProjectResponseDto> collect = projectRepository.findAll().stream()
                .map(projectMapper::mapToDto)
                .collect(Collectors.toList());
        return collect;
    }

    @Override
    public List<ProjectResponseDto> findAll(ProjectRequestDto projectRequestDto) {
        List<ProjectResponseDto> collect = projectRepository.findAll(projectRequestDto.getName(), projectRequestDto.getCreatedAt()).stream()
                .map(projectMapper::mapToDto)
                .collect(Collectors.toList());
        return collect;
    }

    @Transactional
    @Override
    public ProjectResponseDto createProject(ProjectRequestDto projectRequestDto) {
        Project project = projectMapper.mapToDomain(projectRequestDto);
        projectRepository.create(project);
        return projectMapper.mapToDto(project);
    }

    @Transactional
    @Override
    public ProjectResponseDto updateProject(ProjectRequestDto projectRequestDto) {
        log.info("Method: updateProject called");
        Project project = projectMapper.mapToDomain(projectRequestDto);
//        ProjectResponseDto projectResponseDto = projectMapper.mapToDto(projectRepository.updateProject(project));
//        log.debug("input {} output {}", projectRequestDto, projectResponseDto);
        ProjectResponseDto projectResponseDto = projectMapper.mapToDto(projectRepository.updateProject(project));

        return projectResponseDto;
    }

    @Transactional
    @Override
    public String deleteProject(String projectName) {
        return projectRepository.deleteProject(projectName);
    }
}
