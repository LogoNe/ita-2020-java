package cz.smartbrains.ita.service;

import cz.smartbrains.ita.domain.Company;
import cz.smartbrains.ita.model.CompanyWithBranchesWithEmployeesResponseDto;
import cz.smartbrains.ita.model.CompanyWithCompanyBranchesResponseDto;

import java.util.List;

public interface CompanyService {
    /**
     * Finds all the companies with branches
     * @return all companies with branches
     */
    List<CompanyWithCompanyBranchesResponseDto> findAllDto();

    /**
     * Finds employees of a specific company by company ID
     * @param comapnyId specifies a company
     * @return company with branches and employees
     */
    CompanyWithBranchesWithEmployeesResponseDto findEmployeesByCompanyId(Long comapnyId);


    /**
     * Finds a specific company by company ID
     * @param companyId specifies a company
     * @return a specific company
     */
    CompanyWithCompanyBranchesResponseDto findCompanyById(Long companyId);
}
