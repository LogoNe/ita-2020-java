package cz.smartbrains.ita.service.impl;

import cz.smartbrains.ita.domain.Employee;
import cz.smartbrains.ita.mapper.CompanyMapper;
import cz.smartbrains.ita.mapper.EmployeeMapper;
import cz.smartbrains.ita.model.CompanyWithBranchesWithEmployeesResponseDto;
import cz.smartbrains.ita.model.EmployeeRequestDto;
import cz.smartbrains.ita.model.EmployeeResponseDto;
import cz.smartbrains.ita.repository.EmployeeRepository;
import cz.smartbrains.ita.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final EmployeeMapper employeeMapper;

    @Transactional
    @Override
    public EmployeeResponseDto createEmployee(EmployeeRequestDto employeeRequestDto) {
        Employee employee = employeeMapper.mapToDomain(employeeRequestDto);
        return employeeMapper.maptoDto(employeeRepository.createEmployee(employee));
    }

    @Transactional
    @Override
    public EmployeeResponseDto updateEmployee(EmployeeRequestDto employeeRequestDto) {
        return null;
    }

    @Transactional
    @Override
    public void deleteEmployee(EmployeeRequestDto employeeRequestDto) {
        employeeRepository.delete(employeeMapper.mapToDomain(employeeRequestDto));
    }
}
