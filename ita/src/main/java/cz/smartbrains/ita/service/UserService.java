package cz.smartbrains.ita.service;

import cz.smartbrains.ita.domain.Project;
import cz.smartbrains.ita.domain.User;
import cz.smartbrains.ita.model.ProjectResponseDto;
import cz.smartbrains.ita.model.UserRequestDto;
import cz.smartbrains.ita.model.UserResponseDto;

import java.util.List;
import java.util.Optional;

public interface UserService {

//    User login(String login);
//POST('/user', request body will be UserRequestDto, creates the user),

    /**
     * Creates an user based on userRequestDto request
     * @param user user to save
     * @return created user
     */
    UserResponseDto saveUser(UserRequestDto user);

// GET('/user', returns all users in UserResponseDtos)

    /**
     * Finds all users
     * @return List of all the users
     */
    List<UserResponseDto> getAllUsers();

    //GET('/user/{login}, returns UserResponseDto by the users login),

    /**
     * Finds a specific user by login, only one login can exist per user
     * @param login login credential
     * @return user with the specific login
     */
    UserResponseDto findByLogin(String login);

//    PUT('/user accepts UserRequestDtcreated_ato as a requestBody and updates a user having the login as the request has.)

    /**
     * Finds and updates user to match UserRequestDto
     * @param user user to update
     * @return updated user
     */
    UserResponseDto updateUser(UserRequestDto user);

    /**
     * Deletes a user based on a his login
     * @param login
     * @return
     */
    //    DELETE('/user/{login}', which deletes user with the login)
    String delteUser(String login);
}
