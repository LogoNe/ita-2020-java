package cz.smartbrains.ita.mapper;

import cz.smartbrains.ita.domain.Employee;
import cz.smartbrains.ita.model.EmployeeRequestDto;
import cz.smartbrains.ita.model.EmployeeResponseDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {ProjectMapper.class})
public interface EmployeeMapper {
    Employee mapToDomain(EmployeeRequestDto source);
    EmployeeResponseDto maptoDto(Employee source);
}
