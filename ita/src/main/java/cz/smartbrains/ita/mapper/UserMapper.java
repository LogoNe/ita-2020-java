package cz.smartbrains.ita.mapper;

import cz.smartbrains.ita.domain.User;
import cz.smartbrains.ita.model.UserRequestDto;
import cz.smartbrains.ita.model.UserResponseDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = PhoneNumberMapper.class)
public interface UserMapper {
    UserResponseDto mapToResponseDto(User user);
    User mapToDomain(UserRequestDto user);
}
