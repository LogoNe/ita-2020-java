package cz.smartbrains.ita.mapper;

import cz.smartbrains.ita.domain.PhoneNumber;
import cz.smartbrains.ita.model.PhoneNumberDto;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper
public interface PhoneNumberMapper {
    PhoneNumber toDomain(PhoneNumberDto source);
    PhoneNumberDto toDto(PhoneNumber source);
    Set<PhoneNumber> toDomains(Set<PhoneNumberDto> source);
    Set<PhoneNumberDto> toDtos(Set<PhoneNumber> source);
}
