package cz.smartbrains.ita.mapper;

import cz.smartbrains.ita.domain.Company;
import cz.smartbrains.ita.model.*;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {AdressMapper.class, ProjectMapper.class})
public interface CompanyMapper {
    Company mapToDomain(CompanyRequestDto source);
    CompanyResponseDto mapToDto(Company source);
    CompanyWithCompanyBranchesResponseDto mapToCompanyWithBranchesDto(Company source);
    CompanyWithBranchesWithEmployeesResponseDto mapToCompanyWithBranchesWithEmployees(Company source);
}
