package cz.smartbrains.ita.mapper;

import cz.smartbrains.ita.domain.Project;
import cz.smartbrains.ita.model.ProjectRequestDto;
import cz.smartbrains.ita.model.ProjectResponseDto;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper(componentModel = "spring")
public interface ProjectMapper {
    Project mapToDomain(ProjectRequestDto source);
    ProjectResponseDto mapToDto(Project source);
    Set<Project> mapToDomains(Set<ProjectRequestDto> source);
    Set<ProjectResponseDto> mapToDtos(Set<Project> source);
}
