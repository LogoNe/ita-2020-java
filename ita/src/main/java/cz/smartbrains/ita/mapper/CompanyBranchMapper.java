package cz.smartbrains.ita.mapper;

import cz.smartbrains.ita.domain.CompanyBranch;
import cz.smartbrains.ita.model.CompanyBranchRequestDto;
import cz.smartbrains.ita.model.CompanyBranchResponseDto;
import cz.smartbrains.ita.model.CompanyBranchWithEmployees;
import cz.smartbrains.ita.model.CompanyRequestDto;
import org.mapstruct.Mapper;

import java.util.Set;
@Mapper(componentModel = "spring", uses = AdressMapper.class)
public interface CompanyBranchMapper {
    CompanyBranchResponseDto mapToDto(CompanyBranch source);
    CompanyBranch mapToDomain(CompanyBranchRequestDto source);
    CompanyBranchWithEmployees mapToBranchWithEmpl(CompanyBranch source);
}
