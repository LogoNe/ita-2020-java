package cz.smartbrains.ita.mapper;

import cz.smartbrains.ita.domain.Address;
import cz.smartbrains.ita.model.AddressRequestDto;
import cz.smartbrains.ita.model.AddressResponseDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AdressMapper {
    AddressResponseDto mapToDto(Address address);
    Address mapToDomain(AddressRequestDto address);
}
