package cz.smartbrains.ita.client;

import cz.smartbrains.ita.model.AresAddressResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
@FeignClient("ares")
public interface AresMicroClient {
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping(path = "/ares/{ico}")
    AresAddressResponseDto getCompanyInfoByVatId(@PathVariable String ico);
}
