package cz.smartbrains.ita.repository;

import cz.smartbrains.ita.domain.Company;
import cz.smartbrains.ita.domain.CompanyBranch;
import cz.smartbrains.ita.domain.Employee;
import cz.smartbrains.ita.domain.Project;
import cz.smartbrains.ita.mapper.EmployeeMapper;
import cz.smartbrains.ita.model.EmployeeRequestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Employee> findAll(){
        return em.createQuery("SELECT e FROM Employee e").getResultList();
    }

    @Override
    @Transactional
    public Employee createEmployee(Employee employee) {
        em.persist(employee);
        return employee;
    }

    @Override
    @Transactional
    public Employee updateEmployee(Employee emp){
        Employee employee = (Employee) em.createQuery("SELECT p FROM Project p WHERE p.name = :employeeName")
                .setParameter("employeeName", emp.getFirstName())
                .getSingleResult();

        employee.setFirstName(emp.getFirstName());
        employee.setLastName(emp.getLastName());
        employee.setSalary(emp.getSalary());
        employee.setProjects(emp.getProjects());

        return em.merge(employee);
    }

    @Override
    @Transactional
    public void delete(Employee emp) {
        Employee employee = (Employee) em.createQuery("SELECT p FROM Project p WHERE p.name = :employeeName")
                .setParameter("employeeName", emp.getFirstName())
                .getSingleResult();
        em.remove(employee);
    }
}

