package cz.smartbrains.ita.repository;

import cz.smartbrains.ita.domain.Company;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class CompanyRepositoryImpl implements CompanyRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Company findCompanyById(Long companyId) {
        return (Company) em.createQuery("SELECT c FROM Company c WHERE c.id =:companyId")
                .setParameter("companyId", companyId)
                .getSingleResult();
    }

    @Override
    public List<Company> findAll() {
        return em.createQuery("SELECT c FROM Company c")
                .getResultList();
    }

    @Override
    @Transactional
    public void create(Company company) {
        em.persist(company);
    }
}
