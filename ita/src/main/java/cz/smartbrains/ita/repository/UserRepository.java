package cz.smartbrains.ita.repository;

import cz.smartbrains.ita.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
//    @Query(value = "SELECT u from User u where u.login=:login")
//    List<User> fl(String login);
    Optional<User> findByLogin(String login);
    List<User> findAll();
}
