package cz.smartbrains.ita.repository;

import cz.smartbrains.ita.domain.Project;
import cz.smartbrains.ita.model.ProjectRequestDto;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

public interface ProjectRepository {
    List<Project> findAll();

    List<Project> findAll(String name, LocalDateTime createdAfter);

    @Transactional
    void create(Project project);

    @Transactional
    Project updateProject(Project project);

    @Transactional
    String deleteProject(String projectName);

}
