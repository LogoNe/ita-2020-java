package cz.smartbrains.ita.repository;

import cz.smartbrains.ita.domain.Project;
import cz.smartbrains.ita.model.ProjectRequestDto;
import org.springframework.stereotype.Repository;

import javax.annotation.processing.ProcessingEnvironment;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class ProjectRepositoryImpl implements ProjectRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Project> findAll(){
        return entityManager.createQuery("SELECT p FROM Project p").getResultList();
    }


    @Override
    public List<Project> findAll(String name, LocalDateTime createdAfter){
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.name LIKE :projectName")
                .setParameter("projectName", name)
                .getResultList();
    }

    @Override
    @Transactional
    public void create(Project project) {
        entityManager.persist(project);
    }

    @Override
    @Transactional
//    public Project updateProject(Project project){
//        Object projectName = entityManager.createQuery("SELECT p FROM Project p WHERE p.name =:projectName")
//                .setParameter("projectName", project.getName()).getSingleResult();
//        return (Project) entityManager.merge(projectName);
//    }
    public Project updateProject(Project project){
//        Project project = entityManager.find(Project.class, projectRequestDto);
        Project projects = (Project) entityManager.createQuery("SELECT p FROM Project p WHERE p.name = :projectName")
                .setParameter("projectName", project.getName())
                .getSingleResult();

        projects.setEmployees(project.getEmployees());
        projects.setEndDate(project.getEndDate());
        projects.setStartDate(project.getStartDate());
        projects.setPrice(project.getPrice());

        return entityManager.merge(projects);
    }

    @Override
    @Transactional
    public String deleteProject(String projectName){
        Project project = (Project) entityManager.createQuery("SELECT p FROM Project p WHERE p.name = :projectName")
                .setParameter("projectName", projectName)
                .getSingleResult();
        entityManager.remove(project);
        return "Ok";
    }
}
