package cz.smartbrains.ita.repository;

import cz.smartbrains.ita.domain.Company;

import java.util.List;

public interface CompanyRepository {
    Company findCompanyById(Long companyId);

    List<Company> findAll();

    void create(Company company);
}
