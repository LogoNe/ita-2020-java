package cz.smartbrains.ita.repository;

import cz.smartbrains.ita.domain.Company;
import cz.smartbrains.ita.domain.Employee;
import cz.smartbrains.ita.model.EmployeeResponseDto;

import javax.transaction.Transactional;
import java.util.List;

public interface EmployeeRepository {

    List<Employee> findAll();

    @Transactional
    Employee createEmployee(Employee employee);

    @Transactional
    Employee updateEmployee(Employee employee);

    @Transactional
    void delete(Employee employee);
}
