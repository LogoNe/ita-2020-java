package cz.smartbrains.ita.repository;

import cz.smartbrains.ita.domain.Project;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProjectCrudRepo extends CrudRepository<Project, Long> {
    List<Project> findAll();
}
