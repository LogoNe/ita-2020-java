package cz.smartbrains.ita.exception;

import org.springframework.http.HttpStatus;

public class UserNotFoundException extends ItaException {
    public UserNotFoundException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }
}
