package cz.smartbrains.ita.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

@Data
@AllArgsConstructor
public class ItaException extends RuntimeException {
    private HttpStatus status;
    private String message;
}
