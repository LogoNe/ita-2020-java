package cz.smartbrains.ita.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;

import java.sql.SQLException;

public class UserAlreadyExistsException extends ItaException {
    public UserAlreadyExistsException() {
        super(HttpStatus.BAD_REQUEST, "User already exists");
    }
}
