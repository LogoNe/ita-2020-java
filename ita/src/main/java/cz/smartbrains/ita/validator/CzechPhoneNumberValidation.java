package cz.smartbrains.ita.validator;

import lombok.experimental.FieldNameConstants;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CzechPhoneNumberValidator.class)
public @interface CzechPhoneNumberValidation {
    String message() default "Phone number is in invalid format";

    Class<?>[] groups() default {};
    public  abstract Class<? extends Payload>[] payload() default {};
}
