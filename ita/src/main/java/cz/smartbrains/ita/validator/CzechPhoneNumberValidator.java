package cz.smartbrains.ita.validator;

import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class CzechPhoneNumberValidator implements ConstraintValidator<CzechPhoneNumberValidation, String> {

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return s != null && s.matches("^(\\+420|)[0-9]{9}$");
    }

    @Override
    public void initialize(CzechPhoneNumberValidation constraintAnnotation) {

    }
}
