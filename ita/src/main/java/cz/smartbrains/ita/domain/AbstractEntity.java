package cz.smartbrains.ita.domain;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@MappedSuperclass
@Data
public class AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @CreationTimestamp
    private LocalDateTime createdAt;
    @UpdateTimestamp
    private LocalDateTime updatedAt;

//    public String getCreatedAt() {
//        DateTimeFormatter formater = DateTimeFormatter.BASIC_ISO_DATE;
//        String formatedCreatedAt = createdAt.format(formater);
//        return formatedCreatedAt;
//    }
//
//    public String getUpdatedAt() {
//        DateTimeFormatter formater = DateTimeFormatter.BASIC_ISO_DATE;
//        String formatedUpdatedAt = updatedAt.format(formater);
//        return formatedUpdatedAt;
//    }
}

