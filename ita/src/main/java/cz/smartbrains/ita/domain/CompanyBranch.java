package cz.smartbrains.ita.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
public class CompanyBranch extends AbstractEntity {

    String name;

    @OneToOne
    @JoinColumn(name = "address_id")
    Address location;

    @OneToMany
    @JoinColumn(name = "branch_id")
    private Set<Employee> employees;

}
