package cz.smartbrains.ita.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Project extends AbstractEntity{
    private String name;
    private String startDate;
    private String endDate;
    private Long price;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "project_employees",
            joinColumns = @JoinColumn(name = "projects_id"),
            inverseJoinColumns = @JoinColumn(name = "employees_id"))
    private Set<Employee> employees;


    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;
}
