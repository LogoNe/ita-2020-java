package cz.smartbrains.ita.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee extends AbstractEntity {
    String firstName;
    String lastName;
    Long salary;

//    @ManyToMany(mappedBy = "employees",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    private Set<Project> projects;


//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "branch_id")
//    private CompanyBranch companyBranch;
//
//    public CompanyBranch getCompanyBranch() {
//        return companyBranch;
//    }
//
//    public void setCompanyBranch(CompanyBranch companyBranche) {
//        this.companyBranch = companyBranche;
//    }

    @ManyToMany(mappedBy = "employees")
    private Collection<Project> projects;

}