package cz.smartbrains.ita.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    private String street;
    private String houseNumber;
    private String city;
    private String country;
}
