package cz.smartbrains.ita.model;

import lombok.Data;

import java.util.Set;

@Data
public class CompanyBranchWithEmployees {
    private String name;
    private AddressResponseDto location;
    private Set<EmployeeResponseDto> employees;
}
