package cz.smartbrains.ita.model;

import lombok.Data;

import java.util.List;
@Data
public class CompanyWithCompanyBranchesResponseDto {
    private String name;
    private String companyId;
    private String vatId;
    private AddressResponseDto headquaters;
    private List<CompanyBranchResponseDto> companyBranches;
}
