package cz.smartbrains.ita.model;


import cz.smartbrains.ita.validator.CzechPhoneNumberValidation;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;


@Data
public class UserRequestDto {
    @NotNull @Size(max = 20)
    private String firstName;
    @NotNull @Size(max = 20)
    private String lastName;
    @CzechPhoneNumberValidation
    private String phone;
    @NotNull
    @Size(max = 20)
    private String login;

    private Set<PhoneNumberDto> phoneNumbers;
}
