package cz.smartbrains.ita.model;

import cz.smartbrains.ita.domain.Address;
import cz.smartbrains.ita.domain.Employee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.Set;
@Data
@NoArgsConstructor
public class CompanyBranchRequestDto {
    private String name;
    private AddressRequestDto location;
    private Set<EmployeeRequestDto> employees;
}
