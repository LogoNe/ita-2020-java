package cz.smartbrains.ita.model;

import cz.smartbrains.ita.domain.Project;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.ManyToMany;
import java.math.BigInteger;
import java.util.Set;
@Data
@NoArgsConstructor
public class EmployeeRequestDto {
    private String firstName;
    private String lastName;
    private Long salary;
    private Set<ProjectRequestDto> projects;
    private CompanyBranchRequestDto companyBranch;
}
