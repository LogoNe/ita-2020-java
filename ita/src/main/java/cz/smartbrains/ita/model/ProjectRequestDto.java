package cz.smartbrains.ita.model;

import cz.smartbrains.ita.domain.Employee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.tomcat.jni.Local;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.ManyToMany;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;
@Data
@NoArgsConstructor
public class ProjectRequestDto {
    private String name;
    private String startDate;
    private String endDate;
    private Long price;

    private LocalDateTime createdAt;

    private Set<EmployeeRequestDto> employees;

    private CompanyRequestDto company;

}
