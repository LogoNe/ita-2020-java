package cz.smartbrains.ita.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AresAddressResponseDto {
    private String city;
    private String street;
    private String streetNumber;
    private String postalCode;
}
