package cz.smartbrains.ita.model;

import lombok.Data;

@Data
public class PhoneNumberDto {
    private String phoneNumber;
}
