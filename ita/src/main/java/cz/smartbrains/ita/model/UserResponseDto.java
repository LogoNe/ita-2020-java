package cz.smartbrains.ita.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
@Data
public class UserResponseDto {
    private String firstName;
    private String lastName;
    private String phone;
    private String login;
    private LocalDateTime createdAt;

    private Set<PhoneNumberDto> phoneNumbers;
}
