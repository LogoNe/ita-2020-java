package cz.smartbrains.ita.model;

import cz.smartbrains.ita.domain.Address;
import cz.smartbrains.ita.domain.Project;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.Set;
@Data
@NoArgsConstructor
public class CompanyRequestDto {
    private String name;
    private String companyId;
    private String vatId;

    private AddressRequestDto headquaters;

    private Set<ProjectRequestDto> projects;
}
