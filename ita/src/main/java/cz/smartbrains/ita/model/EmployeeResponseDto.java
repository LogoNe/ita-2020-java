package cz.smartbrains.ita.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;
@Data
@NoArgsConstructor
public class EmployeeResponseDto {
    private String firstName;
    private String lastName;
    private Long salary;
}
