package cz.smartbrains.ita.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.tomcat.jni.Local;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Set;
@Data
public class ProjectResponseDto {
    private String name;
    private String startDate;
    private String endDate;
    private Long price;

    private LocalDateTime createdAt;

    private Set<EmployeeResponseDto> employees;

    private CompanyResponseDto company;
}
