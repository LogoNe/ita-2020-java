package cz.smartbrains.ita.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
@Data
@NoArgsConstructor
public class CompanyBranchResponseDto {
    private String name;
    private AddressResponseDto location;
//    private Set<EmployeeResponseDto> employees;
}
