package cz.smartbrains.ita.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AddressResponseDto {
    private String street;
    private String houseNumber;
    private String city;
    private String country;
}
