package cz.smartbrains.ita.model;

import cz.smartbrains.ita.domain.Address;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;
@Data
@NoArgsConstructor
public class CompanyResponseDto {
    private String name;
    private String companyId;
    private String vatId;
    private AddressResponseDto headquaters;
}
