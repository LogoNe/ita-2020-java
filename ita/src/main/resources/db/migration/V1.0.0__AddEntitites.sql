create table user_account
(
    id         int8         not null,
    created_at timestamp,
    first_name varchar(255) not null,
    last_name  varchar(255) not null,
    login      varchar(20)  not null,
    phone      varchar(255),
    primary key (id)
);
alter table if exists user_account
    add constraint UK_login unique (login);

create table phone_number
(
    id           int8 not null,
    phone_number varchar(255),
    id_user      int8,
    primary key (id)
);
alter table if exists phone_number
    add constraint FK_phonenumber_useraccount foreign key (id_user) references user_account;

create table address
(
    id           int8 not null,
    city         varchar(255),
    country      varchar(255),
    house_number varchar(255),
    street       varchar(255),
    primary key (id)
);

create table company
(
    id         int8 not null,
    created_at timestamp,
    updated_at timestamp,
    company_id varchar(255),
    name       varchar(255),
    vat_id     varchar(255),
    address_id int8,
    primary key (id)
);
alter table if exists company
    add constraint FK_company_address foreign key (address_id) references address;

create table company_branch
(
    id         int8 not null,
    created_at timestamp,
    updated_at timestamp,
    name       varchar(255),
    address_id int8,
    company_id int8,
    primary key (id)
);
alter table if exists company_branch
    add constraint FK_companybranch_address foreign key (address_id) references address;
alter table if exists company_branch
    add constraint FK_companybranch_company foreign key (company_id) references company;

create table employee
(
    id         int8 not null,
    created_at timestamp,
    updated_at timestamp,
    first_name varchar(255),
    last_name  varchar(255),
    salary     int8,
    branch_id  int8,
    primary key (id)
);
alter table if exists employee
    add constraint FK_employee_companybranch foreign key (branch_id) references company_branch;

create table project
(
    id         int8 not null,
    created_at timestamp,
    updated_at timestamp,
    end_date   varchar(255),
    name       varchar(255),
    price      int8,
    start_date varchar(255),
    company_id int8,
    primary key (id)
);
alter table if exists project
    add constraint FK_project_company foreign key (company_id) references company;

create table project_employees
(
    projects_id  int8 not null,
    employees_id int8 not null,
    primary key (projects_id, employees_id)
);
alter table if exists project_employees
    add constraint FK_projectemployees_employee foreign key (employees_id) references employee;
alter table if exists project_employees
    add constraint FK_projectemployees_project foreign key (projects_id) references project;


create sequence hibernate_sequence start 1 increment 1;



